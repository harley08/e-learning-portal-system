<?php
	include('authentication/auth.php');
?>
<link rel="stylesheet" href="css/slider.css">
<div id="nav-top">
<div class="body-width-80">
    <?php
		if (!isset($_SESSION['learnOffice_uname'])) {
			echo '<ul>';
			echo '<li>' .
					'<a href="login.php">Login</a>' .
					'</li>' .
					'<li>' .
					'<a href="registration.php">Register</a>' .
					'</li>';
			echo '</ul>';
		} 
		?>
</div>
</div>
<div class="header">
	<div class="body-width-80">
    	<a href="index.php">
        <img src="images/banner.png" alt="logo" width="400px" />
        </a>
    </div>
</div>
<div id="nav-header">
	<ul>
        <li><img src="images/small_icons/images/icon_home.gif" /> <a href="index.php">Home</a></li>
        <li><img src="images/small_icons/media/video.png" width="10px" /> <a href="videos.php">Videos</a></li>
        <li><img src="images/small_icons/images/icon_topic_latest.gif" /> <a href="lessons.php">Lesson Board</a></li>
        <li><img src="images/small_icons/images/icon_team.gif" /> <a href="developers.php">Developers</a></li>
        <?php
			if (isset($_SESSION['learnOffice_uname'])) {
			if ($rowCount_session == 1) {
				echo '
					<li>
					<img src="images/small_icons/images/icon_acp.gif" /> <a href="admin_cpanel.php">Admin CPanel</a>
					</li>
				';
			}
			}
		?>
    </ul>
</div>