<?php
	require('db/db.php');
	session_start()
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Send Award</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
    <?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
            ?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">Choose Award</div>
                <div class="panel-border3" align="center">
                	<?php
						$q_awards = "SELECT * FROM awards";
						$awards = mysql_query($q_awards);
						$rowCount_awards = mysql_num_rows($awards);
						
						for ($i = 1; $i <= $rowCount_awards; $i++) {
							$data_awards = mysql_fetch_assoc($awards);
							echo '<img src="'.$data_awards['award_img_url'].'" /><br>';
							echo '<a href="award_sent.php?receiver='.$_GET['receiver'].'&id='.$data_awards['award_id'].'&name='.$data_awards['award_name'].'&url='.$data_awards['award_img_url'].'" class="btn3">Send '.$data_awards['award_name'].' Award</a><br>';
							echo '<div class="dotted-line"></div><br>';
						}
					?>
                </div>
            </div>
        </div>
    </div>
	<?php
		include('footer/footer.php');
    ?>
</body>
</html>