<?php
	require('db/db.php');
	session_start();
	
	if (!isset($_SESSION['learnOffice_uname'])) {
		header("Location: alert.php");
	}
	
	/* count all messages from inbox */
	$q_countInbox_all = "SELECT * FROM messages WHERE pm_receiver = '".$_SESSION['learnOffice_uname']."' AND pm_type = 'inbox'";
	$r_countInbox_all = mysql_query($q_countInbox_all);
	$countInbox_all = mysql_num_rows($r_countInbox_all);
	
	/* count all unread messages from inbox */
	$q_countUnread = "SELECT * FROM messages WHERE pm_receiver = '".$_SESSION['learnOffice_uname']."' AND pm_type = 'inbox' AND pm_status = 'unread'";
	$r_countUnread = mysql_query($q_countUnread);
	$countUnread = mysql_num_rows($r_countUnread);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Private Messages</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
            ?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
            <?php
						echo '<div class="panel">' .
							 '<div class="title3"><div align="left">Private Messages</div></div>' .
							 	'<div class="panel-border2">';
									$q_inbox = "SELECT * FROM messages WHERE pm_receiver = '".$_SESSION['learnOffice_uname']."' AND pm_type = 'inbox' ORDER BY pm_date DESC";
									$r_inbox = mysql_query($q_inbox);
									$rowCount_inbox = mysql_num_rows($r_inbox);
									for ($i = 1; $i <= $rowCount_inbox; $i++) {
										$data_inbox = mysql_fetch_assoc($r_inbox);
										echo '<div class="line">';
											echo '<table cellspacing="5">';
												echo '<tr>';
													echo '<td valign="top">';
													echo '<img src="avatar/avatar.php?user_name='.$data_inbox['pm_sender'].'" width="30px" height="30px" />';
													echo '</td>';
													echo '<td valign="top">';
													if ($data_inbox['pm_status'] == 'unread') {
														echo '<b>' . $data_inbox['pm_subject'] . '...</b><a href="view_message.php?m=inbox&action=read&id='.$data_inbox['pm_id'].'">read</a>.' . '<br>';
													} else {
														echo $data_inbox['pm_subject'] . '...<a href="view_message.php?m=inbox&action=read&id='.$data_inbox['pm_id'].'">read</a>.<br>';
													}
													echo ''.$data_inbox['pm_sender'].' | '.$data_inbox['pm_date'].' | <font color="#ABABAB">'.$data_inbox['pm_status'].'</font>';
													echo '</td>';
												echo '</tr>';
											echo '</table>';
										echo '</div>';
									}
								echo '</div>' .
							 '</div>';
			?>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>