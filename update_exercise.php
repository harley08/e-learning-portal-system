<?php
	require('db/db.php');
	session_start()
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Update Exercise</title>
<link rel="stylesheet" href="css/styles.css">
</head>
<?php
	$id = $_GET['id'];
	$q_exercise = "SELECT * FROM exercises WHERE exercise_id = $id";
	$exercise = mysql_query($q_exercise);
	$data_exercise = mysql_fetch_assoc($exercise);
	$title = $data_exercise['exercise_name'];
	$content = $data_exercise['exercise_content'];
?>
<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">Update Exercise</div>
                <?php
					if (isset($_POST['update'])) {
						$id = $_GET['id'];
						$n_title = $_POST['n_title'];
						$n_content = $_POST['content'];
						
						$q_update = "UPDATE exercises SET exercise_name = '".$_POST['n_title']."' AND exercise_content = '".$_POST['content']."' WHERE exercise_id = $id";
						$update = mysql_query($q_update);
						
						if ($update) {
							echo '
								<div class="alert-success">Exercise successfully updated!</div>
							';
						} else {
							echo '
								<div class="alert-warning">Unable to update exercise! '. mysql_error() .'</div>
							';
						}
					}
				?>
                <div class="panel-border3">
                	<form method="post" action="">
                    	Title:<br>
                        <input type="text" name="n_title" required value="<?php echo $title ?>" /><br><br>
                        Content:<br>
                        <textarea name="content" required><?php echo $content ?></textarea><br><br>
                        <input type="submit" name="update" value="Update" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>