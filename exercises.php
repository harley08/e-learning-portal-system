<?php
	require('db/db.php');
	session_start()
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Exercises</title>
<link rel="stylesheet" href="css/styles.css">
</head>
<?php
	$lesson_id = $_GET['lesson_id'];
?>
<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">
            	<div align="left">
                <?php echo $_GET['lesson'] ?> > <b>Exercises</b>
                </div>
                </div>
                <?php
					include('viewexercises.php');
				?>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>