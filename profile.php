<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Profile</title>
<link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" href="css/video-styles.css">
<script>
	!window.jQuery && document.write('<script src="css/fancybox/<script src="jquery-1.4.3.min.js"><\/script>');
</script>
<script type="text/javascript" src="css/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="css/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript" src="css/slimbox/jquery.min.js"></script>
<script type="text/javascript" src="css/slimbox/slimbox2.js"></script>
<link rel="stylesheet" href="css/slimbox/slimbox2.css" type="text/css" media="screen" />

<script type="text/javascript">
		$(document).ready(function() {
			/*
			*   Examples - images
			*/

			$("a[rel=example_group]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});

			/*
			*   Examples - various
			*/

			$("#various1").fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});

			$("#various2").fancybox();

			$("#various3").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
			
			});

			$("#various4").fancybox({
				'padding'			: 0,
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'dissolve'
			});
							$("#various5").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
			
			});
$("#various6").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various7").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
			$("#various8").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various9").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various10").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various11").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various12").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various13").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
		});
	</script>
</head>
<?php
	$user = $_GET['user'];
	$query_avatar = "SELECT * FROM users WHERE user_name = '$user'";
	$result_avatar = mysql_query($query_avatar);
	$rowCount_avatar = mysql_num_rows($result_avatar);
	for ($i = 1; $i <= $rowCount_avatar; $i++) {		
		$data_avatar = mysql_fetch_assoc($result_avatar);
		$avatar = $data_avatar['avatar_img'];
	}
?>
<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
            <div class="panel">
            	<div class="title3">Customize Profile</div>
                <div class="panel-border2">
                	<div class="line">
                    <?php
						$query_profile = "SELECT * FROM users WHERE user_name = '".$_GET['user']."'";
						$result_profile = mysql_query($query_profile);
						$data_profile = mysql_fetch_assoc($result_profile);
						$user_id = $data_profile['user_id'];
					?>
                    	<a href="change_profile.php?user_name=<?php echo $user; ?>">Change Profile Picture</a>
                    </div>
                    <div class="line">
                    	<a href="">Update Account Information</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3"><?php echo $_GET['user'] ?>'s Profile</div>
                <div class="panel-border3" align="center">
                	<a href="avatar/avatar.php?user_name=<?php echo $user ?>" rel="lightbox-gallery" title="<?php echo $user ?>"><img src="avatar/avatar.php?user_name=<?php echo $user ?>" width="200px" alt="<?php echo $user ?>'s avatar" class="ava-img" /></a>
                    <?php
					echo '<table>';
					echo '<tr>';
					echo '<td>';
						if (isset($_SESSION['learnOffice_uname'])) {
							if ($_SESSION['learnOffice_uname'] <> $user) {
								echo '						
									<a href="send.php?receiver='.$user.'" class="btn3"><img src="images/small_icons/images/icon_contact.png" /> Send Message</a>
								';
							}
						}
						echo '</td>';
						echo '<td>';
						$q_check_auth = "SELECT * FROM users WHERE user_name = '".$_SESSION['learnOffice_uname']."' AND user_role = 'Administrator'";
						$r_check_auth = mysql_query($q_check_auth);
						$check_auth = mysql_num_rows($r_check_auth);
						
						if ($check_auth == 1) {
							echo '<a href="send_award.php?receiver='.$user.'" class="btn3"><img src="images/small_icons/images/icon_contact.png" /> Send Reward</a>';
						}
						echo '</td>';
						echo '</tr>';
						echo '</table>';
					?>
                </div>
                <div class="panel-border2">
                	<?php
						$query_profile = "SELECT * FROM users WHERE user_name = '".$_GET['user']."'";
						$result_profile = mysql_query($query_profile);
						$data_profile = mysql_fetch_assoc($result_profile);
						
						$fullname = $data_profile['user_fullname'];
						$gender = $data_profile['user_gender'];
						$email = $data_profile['user_email'];
						$user_id = $data_profile['user_id'];
						
						echo '<div class="line">';
						echo '<b>Full Name:</b> ';
						echo $fullname;
						echo '</div>';
						echo '<div class="line">';
						echo '<b>Gender:</b> ';
						echo $gender;
						echo '</div>';
						echo '<div class="line">';
						echo '<b>Email:</b> ';
						echo $email;
						echo '</div>';
						echo '<div class="line">';
						echo 'Awards: ';
						$q_awards = "SELECT * FROM user_awards WHERE user_name = '$user'";
						$awards = mysql_query($q_awards);
						$rowCount = mysql_num_rows($awards);
						if ($rowCount == 0) {
							echo '<b>no awards yet</b>';
						}
						for ($i = 1; $i <= $rowCount; $i++) {
							$data_awards = mysql_fetch_assoc($awards);
							echo ' <img src="'.$data_awards['award_img_url'].'" width="20px" alt="'.$data_awards['award_name'].'"" /> ';
						}
						echo '</div>';
					?>
                </div>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>