<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Register</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    <div id="center">
        	<div class="panel">
            	<div class="title">Registration</div>
                <?php
				if (isset($_POST['username']))
				{
					  $query_no_avatar = "SELECT * FROM user_avatar WHERE avatar_id = 4";
					$result_no_avatar = mysql_query($query_no_avatar);
					$data_no_avatar = mysql_fetch_assoc($result_no_avatar);
					$no_avatar = $data_no_avatar['avatar_img'];
					$no_avatar = addslashes($no_avatar);
					
					$reg_uname = $_POST['username'];
					$reg_pw = $_POST['password'];
					$reg_email = $_POST['email'];
					$reg_fullname = $_POST['fullname'];
					$reg_gender = $_POST['gender'];
					
					$reg_uname = stripslashes($reg_uname);
					$reg_pw = stripslashes($reg_pw);
					$reg_email = stripslashes($reg_email);
					$reg_fullname = stripslashes($reg_fullname);
					$reg_gender = stripslashes($reg_gender);
					
					$reg_uname = mysql_real_escape_string($reg_uname);
					$reg_pw = mysql_real_escape_string($reg_pw);
					$reg_email = mysql_real_escape_string($reg_email);
					$reg_fullname = mysql_real_escape_string($reg_fullname);
					$reg_gender = mysql_real_escape_string($reg_gender);
					
					$reg_check_uname_query = "SELECT * FROM users WHERE user_name LIKE '%".$reg_uname."%'";
					$reg_check_uname_result = mysql_query($reg_check_uname_query);
					$reg_chech_uname_rowCount = mysql_num_rows($reg_check_uname_result);
					
					$reg_user_date_registered = date("Y-m-d H:i:s");
					
					if ($reg_chech_uname_rowCount == 1) {
						echo '
						<div class="alert-warning">
							Username already taken! Please Choose Another One!
						</div>
						';
					}					
					else if ($reg_chech_uname_rowCount == 0)
					{					
					$reg_query = "INSERT INTO users (user_name, user_pass, user_email, user_fullname, user_gender, user_date_registered, user_role, avatar_img) VALUES ('$reg_uname', '".md5($reg_pw)."', '$reg_email', '$reg_fullname', '$reg_gender', '$reg_user_date_registered', 'Member', '{$no_avatar}')";
					
					$reg_result = mysql_query($reg_query);
					
					if ($reg_result) {
						echo '
						<div class="alert-success">Successfull Registered. You can <a href="login.php">login now</a>.</div>
						';
					} else {
						echo '
						<div class="alert-warning">Unable to Register! Please Contact Administrator!</div>
						';
					}
					}
				}
				?>
                <div class="panel-border">
                	<div class="panel-content padding-10" align="center">
                    <form method="post" action="" name="register">
                    	Username:<br>
                        <input type="text" name="username" placeholder="Username" required /><br><br>
                        Password:<br>
                        <input type="password" name="password" placeholder="Password" required /><br><br>
                        E-mail:<br>
                        <input type="email" name="email" placeholder="@" required /><br><br>
                        Gender:<br>
                        <select name="gender">
                        	<option value="gender...">gender...</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select><br><br>
                        Full Name:<br>
                        <input type="text" name="fullname" placeholder="First name Middle name Last name" /><br><br>
                        <input type="submit" name="register" value="Register" /><br><br>
                        Already a member? <a href="login.php">Login</a> now.
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>