<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>User Removed</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">Notification</div>
                <div class="panel-border3" align="center">
                	<?php
						if (isset($_GET['user_id'])) {
							$user_id = $_GET['user_id'];
							$query_remove_user = "DELETE FROM users WHERE user_id = $user_id";
							$result_remove_user = mysql_query($query_remove_user);
							if ($result_remove_user) {
								echo '<h1>';
								echo 'User Successfully Removed!!!';
								echo '</h1>';
							}
						} else {
							echo '<h1>';
							echo 'no user to remove';
							echo '</h1>';
						}
					?>
                </div>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>