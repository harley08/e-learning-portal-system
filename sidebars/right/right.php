<div class="panel">
<div class="title3">Developers Photos</div>
<div class="panel-border3">
<?php
	$query_developers_photos = "SELECT * FROM users WHERE user_role = 'Administrator' ORDER BY user_id DESC";
	$result_developers_photos = mysql_query($query_developers_photos);
	$rowCount_developers_photos = mysql_num_rows($result_developers_photos);
	
	echo '<ul class="slides">';
	for ($i = 1; $i <= $rowCount_developers_photos; $i++) {
		$data_developers_photos = mysql_fetch_assoc($result_developers_photos);
		$developer_fullname = $data_developers_photos['user_fullname'];
		$developer_username = $data_developers_photos['user_name'];
		
		if ($i == 1) {
			$prev = $rowCount_developers_photos;
			$next = 2;
		}
		 else if ($i <> 1 && $i <= $rowCount_developers_photos-1) {
			$prev = $i-1;
			$next = $i+1;
		}
		else {
			$next = 1;
		}
		
		echo '<input type="radio" name="radio-btn" id="img-'.$i.'" checked />';
		echo '<li class="slide-container">';
		echo '<div class="slide">';
		echo '<img src="avatar/avatar.php?user_name='.$developer_username.'" />';		
		echo '</div>';
		echo '<div class="slide">';
		echo '<div class="dev-name">';
		echo $developer_fullname;
		echo '</div>';		
		echo '</div>';
		echo '<div class="nav">';
		echo '<label for="img-'.$prev.'" class="prev">&#x2039;</label>';
		echo '<label for="img-'.$next.'" class="next">&#x203a;</label>';
		echo '</div>';
		echo '</li>';
	}
	echo '</ul>';
?>
</div>
</div>

<div class="panel">
	<div class="title3">User Updates</div>
    <div class="panel-border2">
    	<?php
		
			$query_user_updates = "SELECT * FROM users";
			$result_user_updates = mysql_query($query_user_updates);
			$rowCount_user_updates = mysql_num_rows($result_user_updates);
			
			$query_user_today = "SELECT * FROM users WHERE user_date_registered LIKE '%".date("Y-m-d H:i:s")."%'";
			$result_user_today = mysql_query($query_user_today);
			$rowCount_user_today = mysql_num_rows($result_user_today);
			
			echo '<div class="line">';
			echo '<img src="./images/small_icons/images/icon_members.gif" /> ';
			echo '<b>';
			echo $rowCount_user_updates;
			echo '</b>';
			echo ' total <a href="users.php">users</a>';
			echo '</div>';			
			echo '<div class="line">';
			echo '<img src="./images/small_icons/images/icon_members.gif" /> ';
			echo '<b>';
			echo $rowCount_user_today;
			echo '</b>';
			echo ' users registered today';
			echo '</div>';		
		?>
    </div>
</div>