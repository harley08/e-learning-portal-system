<?php
	require('db/db.php');
	session_start()
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $_GET['exerc_title'] ?></title>
<link rel="stylesheet" href="css/styles.css">
<style type="text/css">
	textarea {
		height: 100px;
	}
</style>
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">
                	<div align="left">
                		<img src="images/small_icons/images/icon_post_target.gif" /> Exercises > <b><?php echo $_GET['exerc_title'] ?></b>
                	</div>
                </div>
                <div class="panel-border3">
                	<?php
						$q = "SELECT * FROM exercises WHERE exercise_id = '".$_GET['id']."'";
						$r = mysql_query($q);
						
						if (!$r) {
							die('Error : ' . mysql_error());
						}
						
						$d = mysql_fetch_assoc($r);
						$exerc_cont = $d['exercise_content'];
						echo '<table>' .
							 '<tr>' .
							 	'<td>' .
								'<img src="avatar/avatar.php?user_name='.$d['user_name'].'" width="15px" height="15px" /> ' . '<b>' . ''.$d['user_name'].'' . '</b> ' . ' | '.$d['exercise_date_posted'].'' .
								'</td>' .
							 '</tr>' .
							 '</table>';
						echo '<p>' . $exerc_cont . '</p>';
					?>
                </div>
                <?php
					if (isset($_SESSION['learnOffice_uname'])) {
						$q_auth = "SELECT * FROM users WHERE user_name = '".$_SESSION['learnOffice_uname']."' AND user_role = 'Administrator'";
						$auth = mysql_query($q_auth);
						
						$rowCount_auth = mysql_num_rows($auth);
						
						if ($rowCount_auth == 1) {
							echo '
								<div class="padding-10-v2">
								<div align="right">
                					Actions: <img src="images/small_icons/icon_delete.gif" width="10px" /> <a href="remove_exercise.php?id='.$_GET['id'].'">Remove</a>
                				</div>
								</div>
							';
						}
					}
 				?>
                <?php
					$id = $_GET['id'];
					$q_comments = "SELECT * FROM exerc_ans WHERE exerc_id = $id";
					$comments = mysql_query($q_comments);
					$rowCount_comments = mysql_num_rows($comments);
					
					if ($rowCount_comments <> 0) {
					echo '<div class="panel">';
					echo '<b>Comment(s):</b> '.$rowCount_comments.'';
					echo '</div>';
					echo '<div class="panel">';
					if (isset($_GET['remove'])) {
						if ($_GET['remove'] == 'comment') {
							$q_deleteComment = "DELETE FROM exerc_ans WHERE ans_id = '".$_GET['ansID']."'";
							$deleteComment = mysql_query($q_deleteComment);
							if ($deleteComment) {
								echo '
									<div class="alert-success">Comment deleted!</div>
								';
							} else {
								echo '
									<div class="alert-warning">Error deleteting comment! '. mysql_error() .'</div>
								';
							}
						}
					}
					echo '<div class="panel-border4">';
					for ($i = 1; $i <= $rowCount_comments; $i++) {
						$data_comment = mysql_fetch_assoc($comments);
						echo '<div class="line">';
							echo '<img src="avatar/avatar.php?user_name='.$data_comment['user_name'].'" width="10px" height="10px" /> <b>'.$data_comment['user_name'].'</b> | '.$data_comment['ans_date_posted'].'';
							$q_check_auth = "SELECT * FROM users WHERE user_name = '".$_SESSION['learnOffice_uname']."' AND user_role = 'Administrator'";
							$check_auth = mysql_query($q_check_auth);
							$rowCount_check_auth = mysql_num_rows($check_auth);
							
							if ($rowCount_check_auth == 1) {
								echo '
									| <a href="?exerc_title='.$_GET['exerc_title'].'&id='.$_GET['id'].'&remove=comment&ansID='.$data_comment['ans_id'].'"><font color="#FF0004">Delete</font></a>
								';
							}
								echo '<div class="comment">';
									echo $data_comment['ans_content'];;
								echo '</div>';
							echo '</div>';
					}
					echo '</div>';
					echo '</div>';
					echo '</div>';
					}
				?>
                <?php
					if (isset($_SESSION['learnOffice_uname'])) {
						if (isset($_POST['comment'])) {
							$comment = $_POST['comment'];
							$exer_id = $_GET['id'];
							$user_name = $_SESSION['learnOffice_uname'];
							$date = date("Y-m-d H:i:s");
							
							$q_comment = "INSERT INTO exerc_ans (exerc_id, user_name, ans_content, ans_date_posted) VALUES ($exer_id, '$user_name', '$comment', '$date')";
							$comment = mysql_query($q_comment);
							
							if ($comment) {
								echo '
									<div class="alert-success">Commented</div>
								';
							} else {
								echo '
									<div class="alert-warning">Error : '.mysql_error().'</div>
								';
							}
						}
						echo '<div class="panel">' .
							'<div class="panel-border3" align="center">' .
							'<form method="post" action="">' .
							'Comment:<br>' .
							'<textarea name="comment"></textarea><br>' .
							'<input type="submit" value="Comment" />' .
							'</form>' .
							'</div>' .
							 '</div>';
					}
				?>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>