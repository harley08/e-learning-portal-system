<?php
	if (!isset($_FILES['userfile'])) {
		echo 'Please select a file!';
	} else {
		try {
			$msg = upload();
			echo $msg;
		}
		catch (Exception $e) {
			echo $e->getMessage();
			echo 'Sorry, could not upload file.';
		}
	}
	function upload() {
		$maxsize = 10000000000;
		
		if ($_FILES['userfile']['error'] == UPLOAD_ERR_OK) {
			if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
				if ($_FILES['userfile']['size'] < $maxsize) {
					if (strpos(mime_content_type($_FILES['userfile']['tmp_name']), "image") === 0) {
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						if (strpos(finfo_file($finfo, $_FILES['userfile']['tmp_name']), "image") === 0) {
							include('db/db.php');
							$imgData = addslashes(file_get_contents($_FILES['userfile']['tmp_name']));
							$user_name = $_GET['user_name'];							
							$query = "UPDATE users SET user_name = '$user_name', avatar_img = '{$imgData}', avatar_name = '{$_FILES['userfile']['tmp_name']}' WHERE user_name = '$user_name'";
							mysql_query($query) or die("Error in Query: " . mysql_error());
							$msg = 'Image Successfully Uploaded in the Database!';
						}
						else 
						{
							$msg = "Uploaded File In Not An Image!";
						}
					}
					else {
						$msg = 'File Exceed The Maximum File Limits!';
					}
				}
				else {
					$msg = "File Not Uploaded Successfully";
				}
			} 
			else {
				$msg = file_upload_error_message($_FILES['userfile']['error']);
			}
			return $msg;
		}
		function file_upload_error_message($error_code) {
			switch ($error_code) {
				case UPLOAD_ERR_INI_SIZE:
            return 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        		case UPLOAD_ERR_FORM_SIZE:
            return 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        		case UPLOAD_ERR_PARTIAL:
            return 'The uploaded file was only partially uploaded';
        		case UPLOAD_ERR_NO_FILE:
            return 'No file was uploaded';
        		case UPLOAD_ERR_NO_TMP_DIR:
            return 'Missing a temporary folder';
        		case UPLOAD_ERR_CANT_WRITE:
            return 'Failed to write file to disk';
        		case UPLOAD_ERR_EXTENSION:
            return 'File upload stopped by extension';
        	default:
            	return 'Unknown upload error';
			}
		}
	}
?>