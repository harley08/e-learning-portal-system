<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Message</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php
		include('header/header.php');
    ?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
            ?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">Message</div>
                <div class="panel-border3">
                	<?php
						$pm_id = $_GET['id'];
						$q_msg_data = "SELECT * FROM messages WHERE pm_id = $pm_id";
						$r_msg_data = mysql_query($q_msg_data);
						$msg_data = mysql_fetch_assoc($r_msg_data);
						if ($msg_data['pm_status'] == 'unread') {
							$pm_id = $_GET['id'];
							$q_update_asRead = "UPDATE messages set pm_status = 'read' WHERE pm_id = $pm_id";
							$r_update_asRead = mysql_query($q_update_asRead);
						}
						echo 'From: ' . '<b>' . $msg_data['pm_sender'] . '</b>' . ' | ';
						echo 'Subject: ' . '<b>' . $msg_data['pm_subject'] . '</b>' . ' | ' . $msg_data['pm_date'] . '<br><br>';
						echo '<div class="panel-border4 padding-10">' . '<p>' . $msg_data['pm_content'] . '</p>' . '</div>';
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
    ?>
</body>
</html>