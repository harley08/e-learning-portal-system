<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Videos</title>
<link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" href="css/video-styles.css">
<script>
	!window.jQuery && document.write('<script src="css/fancybox/<script src="jquery-1.4.3.min.js"><\/script>');
</script>
<script type="text/javascript" src="css/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="css/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript" src="css/slimbox/jquery.min.js"></script>
<script type="text/javascript" src="css/slimbox/slimbox2.js"></script>
<link rel="stylesheet" href="css/slimbox/slimbox2.css" type="text/css" media="screen" />

<script type="text/javascript">
		$(document).ready(function() {
			/*
			*   Examples - images
			*/

			$("a[rel=example_group]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});

			/*
			*   Examples - various
			*/

			$("#various1").fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});

			$("#various2").fancybox();

			$("#various3").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
			
			});

			$("#various4").fancybox({
				'padding'			: 0,
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'dissolve'
			});
							$("#various5").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
			
			});
$("#various6").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various7").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
			$("#various8").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various9").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various10").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various11").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various12").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various13").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
		});
	</script>
</head>

<body>
	<?php
		include('header/header.php');
    ?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">Videos</div>
                <div class="panel-border3">
                	<?php
						if (isset($_SESSION['learnOffice_uname'])) {
							$q_auth = "SELECT * FROM users WHERE user_name = '".$_SESSION['learnOffice_uname']."' AND user_role = 'Administrator'";
							$auth = mysql_query($q_auth);
							$rowCount_auth = mysql_num_rows($auth);
							
							if ($rowCount_auth == 1) {
								echo '
								<center>
								<form method="post" action="" enctype="multipart/form-data">
									<input type="file" name="file" /> <input type="submit" name="upload" value="Upload" />
								</form>
								</center>
								';
								if (isset($_POST['upload'])) {
									$name = $_FILES['file']['name'];
									$temp = $_FILES['file']['tmp_name'];
									
									$url = 'http://localhost/learnoffice/videos/uploaded/'.$name.'';
									
									$uploader = $_SESSION['learnOffice_uname'];									
									$q_upload = "INSERT INTO videos (user_name, v_name, v_url) VALUES ('$uploader', '$name', '$url')";
									$upload = mysql_query($q_upload);
									
									if ($upload) {
										echo '<br> '.$name.' has been uploaded!';
										move_uploaded_file($temp, 'videos/uploaded/'.$name.'');
									} else {
										echo '<br> <center>Cant Uploaded Your File!</center>';
									}
								}
								echo '<br><br>';
 							}
						}
					?>
                    <?php
						$q_videos = "SELECT * FROM videos";
						$videos = mysql_query($q_videos);
						$rowCount_videos = mysql_num_rows($videos);
						
						for ($i = 1; $i <= $rowCount_videos; $i++) {
							$data_video = mysql_fetch_assoc($videos);
							echo '<div class="line">';
							echo '<table cellspacing="10">';
							echo '<tr>';
							echo '<td valign="top">';
							echo '
								<video width="200px" height="200px" controls>
  									<source src="'.$data_video['v_url'].'" type="video/mp4">
  									<object data="'.$data_video['v_url'].'" width="100px" height="200px">
    									<embed width="100px" height="200px" src="'.$data_video['v_url'].'">
  									</object>
								</video>
							';
							echo '<td>';
							echo '<b>' . ''.$data_video['v_name'].'' . '</b>' . '<br><br>';
							echo '<b>' . ''.$data_video['user_name'].'' . '</b>' . ' | '.$data_video['v_date_uploaded'].'';
							echo '</td>';
							echo '</tr>';
							echo '</table>';
							echo '</div>';
						}
					?>
    			</div>
   			 </div>
    	</div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>