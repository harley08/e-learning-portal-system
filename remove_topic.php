<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Remove Topic</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php
    	include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">Alert</div>
                <div class="panel-border3">
                	Are You Sure You Want To Remove Topic?<br><br>
                    <?php
						$topic_id = $_GET['topic_id'];
						$query = "SELECT * FROM topics WHERE topic_id = '$topic_id'";
						$result = mysql_query($query);
						$data = mysql_fetch_assoc($result);
						$topic_title = $data['topic_title'];
						echo '<b>';
						echo $topic_title;
						echo '</b>';
					?>
                    <div align="right">
                    	<a href="topic_removed.php?topic_id=<?php echo $topic_id ?>&action=remove" class="btn">Yes</a>  <a href="topic_removed.php?topic_id=<?php echo $topic_id ?>&action=dont_remove" class="btn">No</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    	include('footer/footer.php');
	?>
</body>
</html>