<?php
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Alert</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php
	echo '
		<div class="body-width-80">
		<div id="center">
		<div class="panel">
	';
		if (isset($_SESSION['learnOffice_uname'])) {
			echo '
				<div class="title3">Alert</div>
				<div class="panel-border3" align="center"> <img src="images/small_icons/alert-warning.gif"> Sorry! You dont have the rights to access this page!</div><br>
				<center>
				← <a href="index.php">Back to Home</a>
				</center>
			';
		} else {
			echo '
				<div class="title3">Alert</div>
				<div class="panel-border3" align="center"> <img src="images/small_icons/alert-warning.gif"> Sorry! You dont have the rights to access this page!</div><br>
				<center>
				← <a href="index.php">Back to Home</a>
				</center>
			';
		}
	echo '
		</div>
		</div>
		</div>
	';
	?>
</body>
</html>