<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Update Topic</title>
<link rel="stylesheet" href="css/styles.css">
</head>
<?php
	$topic_id = $_GET['topic_id'];
	$query_topic = "SELECT * FROM topics WHERE topic_id = $topic_id";
	$result_topic = mysql_query($query_topic);
	$data_topic = mysql_fetch_assoc($result_topic);
	$topic_title = $data_topic['topic_title'];
	$topic_content = $data_topic['topic_content'];
?>
<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">Update Topic</div>
                <?php
					if (isset($_POST['topic_title'])) {
						$new_topic_title = $_POST['topic_title'];
						$new_topic_content = $_POST['topic_content'];
						
						$query_update_topic = "UPDATE topics SET topic_title = '$new_topic_title', topic_content = '$new_topic_content' WHERE topic_id = $topic_id";
						$result_update_topic = mysql_query($query_update_topic);
						if ($result_update_topic) {
							echo '
								<div class="alert-success">
									Topic Successfully Updated! Back to Your Topic to Refresh!
								</div>
							';
						}
					}
				?>
                <div class="panel-border3">
                	<form method="post" action="">
                    	Topic Title:<br>
                        <input type="text" name="topic_title" value="<?php echo $_GET['topic'] ?>" required></input><br><br>
                        Topic Content:<br>
                        <textarea name="topic_content"><?php echo $topic_content ?></textarea><br><br>
                        <input type="submit" value="Update Topic" /> | <img src="images/small_icons/images/icon_topic_attach.gif" /> <a href="upload_screenshot.php">Attach Image/ScreenShot</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>