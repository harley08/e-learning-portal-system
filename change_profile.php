<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Change Account Profile Picture</title>
<link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" href="css/video-styles.css">
<script>
	!window.jQuery && document.write('<script src="css/fancybox/<script src="jquery-1.4.3.min.js"><\/script>');
</script>
<script type="text/javascript" src="css/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="css/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript" src="css/slimbox/jquery.min.js"></script>
<script type="text/javascript" src="css/slimbox/slimbox2.js"></script>
<link rel="stylesheet" href="css/slimbox/slimbox2.css" type="text/css" media="screen" />

<script type="text/javascript">
		$(document).ready(function() {
			/*
			*   Examples - images
			*/

			$("a[rel=example_group]").fancybox({
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
			});

			/*
			*   Examples - various
			*/

			$("#various1").fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});

			$("#various2").fancybox();

			$("#various3").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
			
			});

			$("#various4").fancybox({
				'padding'			: 0,
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'dissolve'
			});
							$("#various5").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
			
			});
$("#various6").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various7").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
			$("#various8").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various9").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various10").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various11").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various12").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
$("#various13").fancybox({
				'autoScale'			: true,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
	
			});
		});
	</script>
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">Change Account Profile Picture</div>
                <div class="panel-border3">
                	Please Choose Your File and Click Submit.<br>
                    <form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>?user_name=<?php echo $_GET['user_name']; ?>" method="post">
                    	<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
                        <input type="file" name="userfile" />
                        <input type="submit" value="Submit" />
                    </form>
                    <?php
						include('check_file/upload_file.php');
					?><br><br><br>
                    <center>
                    <b>Current Avatar</b><br><br>
                    	<a href="avatar/avatar.php?user_name=<?php echo $_SESSION['learnOffice_uname'] ?>" rel="lightbox-gallery" title="<?php echo $_SESSION['learnOffice_uname'] ?>'s Avatar">
                        <img src="avatar/avatar.php?user_name=<?php echo $_SESSION['learnOffice_uname'] ?>" class="ava-img" width="200px" />
                        </a>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>