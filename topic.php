<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<link rel="stylesheet" href="css/styles.css">
</head>
<body>
	<?php
		$lesson_id = $_GET['lesson_id'];
		$q_lesson  = "SELECT * FROM lessons WHERE lesson_id = $lesson_id";
		$r_lesson = mysql_query($q_lesson);
		$data_lesson = mysql_fetch_assoc($r_lesson);
		
		$lesson_category = $data_lesson['lesson_category'];
		
		$lesson_title = $data_lesson['lesson_title'];
	?>
	<?php
		include('header/header.php');
	?>
    
    <div class="body-width-80">
    
    	<div id="left">
        
        	<?php
				include('sidebars/left/left.php');
			?>
        
        </div>
        
        <div id="right">
        	
            <?php
				include('sidebars/right/right.php');
			?>
            
        </div>
        
        <div id="center">
        	<div class="panel">
            	<div class="title3">
					<div align="left">
						<?php echo $lesson_category . ' > ' . '<b>' . $lesson_title . '</b>' ?>
                	</div>
                </div>
                </div>
                	<?php
						include('topics/view_topics/view_topics.php');
					?>            
            
        </div>
    
    </div>
    
    <?php
		include('footer/footer.php');
	?>
</body>
</html>