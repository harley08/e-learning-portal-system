<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Upload ScreenShots</title>
<link rel="stylesheet" href="css/styles.css">
<style type="text/css">
	textarea {
		width: 200px;
		height: 50px;	
	}
</style>
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">Upload ScreenShot</div>
                <div class="panel-border3">
                	Please Choose Your ScreenShot And Submit.<br>
                    <form enctype="multipart/form-data" method="post">
                    	<input type="hidden" name="MAX_FILE_SIZE" />
                        <input type="file" name="userfile" />
                        <input type="submit" value="Submit" />
                    </form>
                    <?php
						include('upload/screenshot/uploadscreenshot.php');
						echo '<br><br>';
						
						$rec_limit = 10;
						
						/* Get total number of records of screenshots */
						$query_countRecord = "SELECT count(ss_id) FROM screenshots";
						$result_countRecords = mysql_query($query_countRecord);
						
						if (!$result_countRecords) {
							die("Could not get the number of records: " . mysql_error());
						}
						
						$row = mysql_fetch_array($result_countRecords, MYSQL_NUM);
						$rec_count = $row[0];
						
						if (isset($_GET['page'])) {
							$page = $_GET['page'] + 1;
							$offset = $rec_limit * $page;
						} else {
							$page = 0;
							$offset = 0;
						}
						
						$left_rec = $rec_count - ($page * $rec_limit);
						
						$query_ss_data = "SELECT * FROM screenshots ORDER BY ss_id DESC LIMIT $offset, $rec_limit";
						$result_query_ss_data = mysql_query($query_ss_data);
						
						if (!$result_query_ss_data) {
							die("Could not get data: " . mysql_error());
						}
						
						while ($row = mysql_fetch_array($result_query_ss_data, MYSQL_ASSOC)) {
							echo '<div class="line">';
							echo '<table cellspacing="10">';
								echo '<tr>';
									echo '<td>';
									echo '<img src="photo.php?ss_id='.$row['ss_id'].'" width="100px" height="100px;" class="ava" />';
									echo '</td>';
									echo '<td>';
									echo 'SS ID: <b>'.$row['ss_id'].'</b><br>';
									echo 'File Name : ';
									echo '<b>';
									echo $row['ss_name'];
									echo '</b>';
									echo '<br>';
									echo 'Uploader : ';
									echo '<b>';
									echo $row['user_uploader'];
									echo '</b>';
									echo '<br>';
									echo '<b>Image Code</b> <br>';
									echo '<textarea><img src="photo.php?filename='.$row['ss_name'].'&uploader='.$row['user_uploader'].'&ss_id='.$row['ss_id'].'"></textarea>';
									echo '</td>';
								echo '</tr>';
							echo '</table>';
							echo '</div>';
						}
						
						echo '<br><br>'; 
						echo '<center>';
						if ($page > 0) {
							$last = $page - 2;
							echo '<a href="?page='.$last.'" class="page-btn">See Previous</a> ';
							echo '<a href="?page='.$page.'" class="page-btn">See More</a> ';
						} else if ($page == 0) {
							echo '<a href="?page='.$page.'" class="page-btn">See More</a> ';
						} else if ($left_rec < $rec_limit) {
							$last = $page -2;
							echo '<a href="?page='.$last.'" class="page-btn">See Previous</a> ';
						}
						echo '</center>';
					?>
                </div>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>