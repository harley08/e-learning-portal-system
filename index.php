<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>HTech Learning</title>
<link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" href="css/slider.css">
<link rel="shortcut icon" href="/images/shortcut icon.png">
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    
    <div id="left">
    	<?php
			include('sidebars/left/left.php');
		?>     
    </div>
    
    <div id="right">
    	<?php
			include('sidebars/right/right.php');
		?>
    </div>
    
    <div id="center">
    	<div class="panel">
        <div class="panel-bg-white">
        <div class="table">
        	<img src="images/minscat-logo.png" width="220px" class="img" />
            <div class="table-td-left">
            <h2><font color="#002CFF">WELCOME TO</font> HTECH LEARNING SITE!</h2>
            <hr></hr>
            <h3><b>HTech Learning</b> is an high-powered e-learning website, use to give information, tutorials, or learnings about Microsoft office Word, Powerpoint, Excell, and Information Tech matters. It helps the user to be more productive when it comes to office works and gives ideas and learnings about Infomation Technology matters.</h3>
            <hr></hr>
            </div><br><br>     
            Welcome to our high-powered E-Learning Website.
Our goal in HTECH LEARNING are:
		<div id="nav-check">
        <ul>
        	<li>
            	Enables users to be more productive in office works.
            </li>
            <li>
            	Give basic knowledge about the Microsoft Office Tools.
            </li>
            <li>
            	Gives techniques for a fast office works.
            </li>
            <li>
            	Gives basic knowledge about Infomation Technology Database Management.
            </li>
            <li>
            	Have a communication between the professional users and the beginner.
            </li>
        </ul>
        </div>
        </div>
        </div>
        </div>
        <div class="panel">
        <div class="title3">
        <table>
        <tr>
        <td>
        <img src="images/small_icons/images/topic_read_hot.gif" />
        </td>
        <td>
        Lesson Board
        </td>
        </tr>
        </table>
        </div>
        <div class="panel-border2">
        	<div class="lesson-categ-title"><img src="images/small_icons/images/icon_bump.gif" /> Microsoft Office 2013</div>
            <div class="lessons-border">
            	<?php
					include('index/show_lessons/lessons/show_lessons.php');
				?>
            </div>
            <div class="lesson-categ-title"><img src="images/small_icons/images/icon_bump.gif" /> Microsoft Office 2010</div>
            <div class="lessons-border">
            	<?php
					include('index/show_lessons_10/lessons_10/show_lessons_10.php');
				?>
                <div class="lesson-categ-title"><img src="images/small_icons/images/icon_bump.gif" /> Information Technology</div>
                <?php
	$show_lessons_query = "SELECT * FROM lessons WHERE lesson_category = 'Information Technology'";
	$show_lessons_result = mysql_query($show_lessons_query);
	$show_lessons_rowCount = mysql_num_rows($show_lessons_result);
	for ($i = 1; $i <= $show_lessons_rowCount; $i++) {
		$show_lessons_data = mysql_fetch_assoc($show_lessons_result);
		$lesson_id = $show_lessons_data['lesson_id'];
		
		/* count number of topics */
		$query_topic_rowCount = "SELECT * FROM topics WHERE lesson_id = '$lesson_id'";
		$result_topic_rowCount = mysql_query($query_topic_rowCount);
		$topic_rowCount = mysql_num_rows($result_topic_rowCount);
		
		/* count number of exercises */
		$q_c_e = "SELECT * FROM exercises WHERE lesson_id = $lesson_id";
		$c_e = mysql_query($q_c_e);
		$e_count = mysql_num_rows($c_e);
		
		echo '
		<div class="lesson">
		<table>
		<tr>
		<td>
		<img src="images/small_icons/images/forum_link.gif" />
		</td>		
		<td>
		 <a href="topic.php?lesson_id='.$show_lessons_data['lesson_id'].'">
		';
		echo $show_lessons_data['lesson_title'];
		echo '
		</a>
		[ <img src="./images/small_icons/images/icon_post_target.gif" /> <b>';
		echo $topic_rowCount;
		echo '
		</b> Topics] [ <img src="images/small_icons/media/video.png" width="7px" /> <b>'.$e_count.'</b> <a href="exercises.php?lesson='.$show_lessons_data['lesson_title'].'&lesson_id='.$show_lessons_data['lesson_id'].'">Exercises</a>]
		</td>
		</tr>
		</table>
		</div>
		';
	}
?>
            </div>            
        </div>
    </div>
    </div>
    
    </div>
    
    <?php
		include('footer/footer.php');
	?>
</body>
</html>