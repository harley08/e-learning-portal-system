<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Successfully Logged In</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div id="center">
    	<div id="panel">
        	<div class="title3">Notification</div>
        	<div class="panel-border3" align="center">
            	<font color="#9A9A9A"><h2><img src="css/images/check_small.gif" /> Successfully Logged Out!</h2></font>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>