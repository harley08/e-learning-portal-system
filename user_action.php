<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>
	<?php
					$action = $_GET['action'];
					if ($action == 'remove') {
						echo 'Remove User';
					}
					else if ($action == 'update_info') {
						echo 'Update User Info';
					}
					else if ($action == 'update_role') {
						echo 'Update User Role';
					}
				?>
</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">
                <?php
					$action = $_GET['action'];
					if ($action == 'remove') {
						echo 'Remove User';
					}
					else if ($action == 'update_info') {
						echo 'Update User Info';
					}
					else if ($action == 'update_role') {
						echo 'Update User Role';
					}
				?>
                </div>
                <div class="panel-border3">
                	<?php
					
					$user_id = $_GET['user_id'];
					
					$query = "SELECT * FROM users WHERE user_id = $user_id";
					$result = mysql_query($query);
					$data = mysql_fetch_assoc($result);
					
					$username = $data['user_name'];
					$fullname = $data['user_fullname'];
					$gender = $data['user_gender'];
					$avatar = $data['avatar_img'];
					$email = $data['user_email'];
					$id = $data['user_id'];
					$role = $data['user_role'];
					
					echo '<table cellspacing="15">';
					echo '<tr>';
					echo '<td>';
						echo '<img src="data:image/jpeg;base64,';
						echo base64_encode($avatar);
						echo '" width="100px" />';
					echo '</td>';
					echo '<td>';
						echo '<b>Username:</b> ';
						echo $username;
						echo '<br>';
						echo '<b>Full Name:</b> ';
						echo $fullname;
						echo '<br>';
						echo '<b>Email:</b> ';
						echo $email;
						echo '<br>';
						echo '<b>Gender:</b> ';
						echo $gender;
						echo '<br>';
						echo '<b>Developer ID:</b> ';
						echo $id;
					echo '</td>';
					echo '</tr>';
					echo '</table>';
					
					
					$action = $_GET['action'];
					if ($action == 'remove') {
								echo '
									<h2>Are You Sure You Want To Remove This User?</h2>
									<div align="right"><br>
										<a href="user_removed.php?user_id='.$id.'&action=remove_user" class="btn">Yes</a> <a href="index.php" class="btn">No</a>
									</div>
								';
					}
					else if ($action == 'update_info') {
						echo '
							<form method="post" action="">
								Username:<br>
								<input type="text" name="username" required value="'.$username.'" /><br><br>
								Full Name:<br>
								<input type="text" name="fullname" required value="'.$fullname.'" /><br><br>
								Email:<br>
								<input type="text" name="email" value="'.$email.'" required /><br><br>
								Gender:<br>
								<select name="gender">
									<option value="
									';
									echo $gender;
									echo '
									">'.$gender.'</option>
									<option value="Male">Male</option>
									<option value="Female">Female</option>
								</select><br><br>
								<input type="submit" value="Update Info" />
							</form>
						';
						if (isset($_POST['username'])) {
							$new_username = $_POST['username'];
							$new_fullname = $_POST['fullname'];
							$new_email = $_POST['email'];
							$new_gender = $_POST['gender'];
							$query_update_info = "UPDATE users set user_name = '$new_username', user_fullname = '$new_fullname', user_email = '$new_email', user_gender = '$new_gender' WHERE user_id = $id";
							$result_update_info = mysql_query($query_update_info);
							echo '<br>';
							if ($result_update_info) {
								echo '
								 <img src="css/images/check_small.gif" /> User Info Successfully Updated!
								';
							} else {
								echo '
								Cant Update User Info. Contact Administrator!
								';
							}
						}
					}
					else if ($action == 'update_role') {
						echo '
							<form method="post" action="">
								<select name="role">
									<option value="'.$role.'">'.$role.'</option>
									<option value="Administrator">Administrator</option>
									<option value="Contributor">Contributor</option>
									<option value="Member">Member</option>
								</select> <input type="submit" value="Update Role" />
							</form>
						';
						echo '<br>';
						if (isset($_POST['role'])) {							
						$new_role = $_POST['role'];
							$query_update_role = "UPDATE users SET user_role = '$new_role' WHERE user_id = $id";
							$result_update_role = mysql_query($query_update_role);
							if ($result_update_role) {
								echo '<img src="css/images/check_small.gif" /> Successfull Updated User Role!';
							} else {
								echo '
									Cant Update User Role. Contact Administrator!
								';
							}
						}
					}
				?>
                </div>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>