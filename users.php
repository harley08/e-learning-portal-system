<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Users</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
                	<div class="title3">Users</div>
                    <div class="panel-border2">
                    	<?php
							$query_developers = "SELECT * FROM users";
							$result_developers = mysql_query($query_developers);
							$rowCount_developers = mysql_num_rows($result_developers);
							for ($i = 1; $i <= $rowCount_developers; $i++) {
								$data_dev = mysql_fetch_assoc($result_developers);
								
								$dev_uname = $data_dev['user_name'];
								$dev_name = $data_dev['user_fullname'];
								$dev_email = $data_dev['user_email'];
								$dev_gender = $data_dev['user_gender'];
								$dev_id = $data_dev['user_id'];
								$dev_avatar = $data_dev['avatar_img'];
								echo '<div class="line">';
									echo '<table cellspacing="10">';
										echo '<tr>';
											echo '<td valign="top">';
											echo '<div class="ava-content">';
											echo '<img src="data:image/jpeg;base64,';
											echo base64_encode($dev_avatar);
											echo '" class="ava-list" />';
											echo '</div>';
											echo '</td>';
											echo '<td valign="top">';
											echo '<img src="images/small_icons/images/icon_ucp.gif" /> <b>Username:</b> ';
											echo '<a href="profile.php?user='.$dev_uname.'">' . $dev_uname . '</a>';
											echo '<br>';
											echo '<img src="images/small_icons/images/icon_ucp.gif" /> <b>Full Name:</b> ';
											echo $dev_name;
											echo '<br>';
											echo '<img src="images/small_icons/images/icon_ucp.gif" /> <b>Email:</b> ';
											echo $dev_email;
											echo '<br>';
											echo '<img src="images/small_icons/images/icon_ucp.gif" /> <b>Gender:</b> ';
											echo $dev_gender;
											echo '<br>';
											echo '<img src="images/small_icons/images/icon_ucp.gif" /> <b>Role:</b> ';
											echo $data_dev['user_role'];
											echo '<br>';
											echo '<div class="admin-show">';											
											echo '<br>';
											if ($dev_uname == $_SESSION['learnOffice_uname']) {
											
											} else {
												echo '<img src="images/small_icons/icon_delete.gif" width="10px" /> <a href="user_action.php?action=remove&user='.$dev_uname.'&user_id='.$dev_id.'"><font color="#FF0004">Remove User</font></a> <small>|</small> '; 	
											}
											echo '<img src="images/small_icons/icon_edit.gif" width="10px" /> <a href="user_action.php?action=update_info&user='.$dev_uname.'&user_id='.$dev_id.'"><font color="#4200FF">Update Info</font></a> <small>|</small> <img src="images/small_icons/icon_edit.gif" width="10px" /> <a href="user_action.php?action=update_role&user='.$dev_uname.'&user_id='.$dev_id.'"><font color="#929292">Update Role</font></a>';
											echo '</div>';
											echo '</td>';
										echo '</tr>';
									echo '</table>';
								echo '</div>';
							}
                        ?>
                    </div>
                </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>