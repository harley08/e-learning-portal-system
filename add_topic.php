<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Add New Topic</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php
		$board = $_GET['topic_board'];
	?>
	<?php
		include('header/header.php');
	?>
    
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3"><?php echo $board ?> > <b>Add New Topic</b></div>
                <?php
				if (isset($_POST['topic_title']))
				{
					$topic_title = $_POST['topic_title'];
					$topic_content = $_POST['topic_content'];
					
					$topic_title = stripslashes($topic_title);
					$topic_content = stripslashes($topic_content); 
					
					$topic_title = mysql_real_escape_string($topic_title);
					$topic_content = mysql_real_escape_string($topic_content); 
					
					$topic_date_posted = date("Y-m-d H:i:s");
					
					$topic_query = "INSERT INTO topics (lesson_id, user_name, topic_title, topic_content, topic_date_posted) VALUES ('".$_GET['lesson_id']."', '".$_SESSION['learnOffice_uname']."', '$topic_title', '$topic_content', '$topic_date_posted')";
					$topic_result = mysql_query($topic_query);
					if ($topic_result) {
						echo '
						<div class="alert-success">New Topic Successfully Added!</div>
						';
					} else {
						echo '
						<div class="alert-warning">New Topic Successfully Added!</div>
						';
					}
				}
				?>
                <div class="panel-border3">
                <form method="post" action="">
                	Topic Title:<br>
                    <input type="text" name="topic_title" placeholder="Topic title here..." required /><br><br>
                    Content:<br>
                    <textarea name="topic_content" placeholder="content here..." required></textarea><br><br>
                    <input type="submit" name="add" value="Add Topic" /> | <img src="images/small_icons/images/icon_topic_attach.gif" /> <a target="new" href="upload_screenshot.php">Attach Image/ScreenShot</a>
                </form>
                </div>
            </div>
        </div>
    </div>
    
    <?php
		include('footer/footer.php');
	?>
</body>
</html>