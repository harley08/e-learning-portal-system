<?php
	require('db/db.php');
	session_start();
	if (!isset($_SESSION['learnOffice_uname'])) {
		header("Location: alert.php");
	}
	else if (isset($_SESSION['learnOffice_uname'])) {
		$session_name2 = $_SESSION['learnOffice_uname'];
		$query_session2 = "SELECT * FROM users WHERE user_name = '".$_SESSION['learnOffice_uname']."' AND user_role <> 'Administrator'";
		$result_session2 = mysql_query($query_session2);
		$rowCount_session2 = mysql_num_rows($result_session2);
		if ($rowCount_session2 == 1) {
		header("Location: alert.php");
	}
	}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Admin Control Panel</title>
<link rel="stylesheet" href="css/styles.css"> 
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    
    <div id="left">
    	<?php
			include('sidebars/left/left.php');
		?>
    </div>
    
    <div id="right">
    	<?php
			include('sidebars/right/right.php');
		?>
    </div>
    
    <div id="center">
    	<div class="panel">
        	<div class="title3">Admin Control Panel</div>
            <div class="panel-border3">
            <br>
            	<a href="?add_lesson=add" class="btn">Add Lesson on Board</a>
                <a href="?awards" class="btn">Awards</a>
                <br><br>
            </div>
        </div>
        <?php
					include('admin_cpanel/add_lesson/lesson/add_lesson.php');
		?>
        <?php
			if (isset($_GET['add'])) {
				if (isset($_POST['upload'])) {
									$name = $_FILES['file']['name'];
									$temp = $_FILES['file']['tmp_name'];
									
									$url = 'awards/imgs/'.$name.'';
									$award_name = $_POST['award_name'];
									
									$uploader = $_SESSION['learnOffice_uname'];									
									$q_upload = "INSERT INTO awards (award_name, award_img_url, award_img_name) VALUES ('$award_name', '$url', '$name')";
									$upload = mysql_query($q_upload);
									
									if ($upload) {
										echo '
										<div class="alert-success">'.$name.' has been uploaded!</div>';
										move_uploaded_file($temp, 'awards/imgs/'.$name.'');
									} else {
										echo '<div class="alert-warning">Cant Uploaded Your File!</div>';
									}
								}
				echo '<div class="panel">' .
					 '<div class="panel-border3" align="center">' .
					 '
					 <div align="right"><a href="?awards">x</a></div>
					 	<form method="post" action="" enctype="multipart/form-data">
						<div align="left">Add New Award:</div><br>
							<input type="text" name="award_name" placeholder="award name here ..." />
							<input type="file" name="file" />
							<input type="submit" name="upload" value="Upload" />
						</form>';
						
					 echo '</div>' .
					 '</div>';
			}
		?>
        <?php
					if (isset($_GET['awards'])) {
						echo '<div class="panel">' .
							 '<div class="title3">Awards <a href="admin_cpanel.php">X</a></div>' .
							 	'<div class="panel-border3">' .
									'<table width="100%">' .
									'<tr>' .
									'<td align="left">' .
							 		'Awards:' .
									'</td>' .
									'<td align="right">' .
									'<a href="?awards&add" class="btn3">Add New Award</a>' .
									'</td>' .
									'</tr>' .
									'</table>';
									$q_awards = "SELECT * FROM awards";
									$awards = mysql_query($q_awards);
									$rowCount_awards = mysql_num_rows($awards);
									
									for ($i = 1; $i <= $rowCount_awards; $i++) {
										$data_awards = mysql_fetch_assoc($awards);
										echo '<img src="'.$data_awards['award_img_url'].'" width="20px" /> - '.$data_awards['award_name'].'<br>';
									}
							 	echo '</div>' .
							 '</div>';
					}
				?>
    </div>
    
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>