<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Remove Exercises</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php	
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="title3">Remove</div>
            <div class="panel-border3">
            	<?php
					if (isset($_GET['id'])) {
						if (isset($_GET['remove'])) {
							if ($_GET['remove'] == 'true') {
								$q_remove = "DELETE FROM exercises WHERE exercise_id = '".$_GET['id']."'";
								$remove = mysql_query($q_remove);
								
								if ($remove) {
									echo 'Exercise successfully removed!';
								}
							}
						} else {
						echo '
						<h1>Are You Sure You Want To Remove This Exercise?</h1>
						<br>
						<a href="?id='.$_GET['id'].'&remove=true" class="btn3">Yes</a>
						<br>
						<a href="index.php" class="btn3">No</a> 
						';
						}
					} else {
						echo 'no exercise to be remove';
					}
                ?>
            </div>
        </div>
    </div>
    <?php 
		include('footer/footer.php');
	?>
    </body>
</body>
</html>