<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Send Message</title>
<link rel="stylesheet" href="css/styles.css">
<style type="text/css">
	textarea {
		height: 100px;
	}
</style>
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        	<div class="panel">
            	<div class="title3">Send Message</div>
                <?php
					if (isset($_POST['subject'])) {
						$sender = $_SESSION['learnOffice_uname'];
						$receiver = $_GET['receiver'];
						$subject = $_POST['subject'];
						$msg = $_POST['message_content'];
						$pm_date = date("Y-m-d H:i:s");
						$q_send = "INSERT INTO messages (pm_type, pm_sender, pm_receiver, pm_subject, pm_content, pm_date, 	pm_status) VALUES ('inbox', '$sender', '$receiver', '$subject', '$msg', '$pm_date', 'unread')";
						$send = mysql_query($q_send);
						
						if ($send) {
							echo '<div class="alert-success">Message sent!</div>';
						} else {
							echo '<div class="alert-warning">Message not sent! ' . mysql_error() . '</div>';
						}
					}
				?>
                <div class="panel-border3">
                <?php
					$sender = $_SESSION['learnOffice_uname'];
					$receiver = $_GET['receiver'];
					echo '<form method="post" action="">';
					echo '<b>Sender:</b> ' . $sender . '<br>' . 
						 '<b>To:</b> ' . $receiver . '<br><br>' .
						 'Subject:<br>' .
						 '<input type="text" name="subject" placeholder="subject here..." required /><br><br>' .
						 'Message: <br>' .
						 '<textarea name="message_content" placeholder="message here..." required></textarea><br><br>' .
						 '<input type="submit" value="Send" />';
				?>
                </div>
            </div>
            <div class="panel">
            	<div class="padding-10">
                	<a href="">Add to Archieve</a> | <a href="">Add to Trashed</a>
                </div>
            </div>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>