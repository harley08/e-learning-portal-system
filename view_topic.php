<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>View Topic</title>
<link rel="stylesheet" href="css/styles.css">
</head>
<?php
	$lesson_id = $_GET['lesson_id'];
	
	$q_topic = "SELECT * FROM topics WHERE lesson_id = ";
?>
<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        <div class="panel">
        	<div class="title3">
            	<div align="left">
					<img src="images/small_icons/images/subforum_read.gif" /> <?php echo $_GET['lesson_title'] ?> > <b><?php echo $_GET['topic'] ?></b>
            	</div>
            </div>
            <div class="panel-border3">
            	<?php
					$view_topic_query = "SELECT * FROM topics WHERE topic_id = '".$_GET['topic_id']."'";
					$view_topic_result = mysql_query($view_topic_query);
					$view_topic_data = mysql_fetch_assoc($view_topic_result);
					$topic_user = $view_topic_data['user_name'];
					
					echo '<p>';
					echo $view_topic_data['topic_content'];
					echo '<br><br>';
					echo 'Posted by: ';
					echo '<img src="avatar/avatar.php?user_name='.$topic_user.'" width="15px" class="ava" /> ';
					echo '<small>';
					echo '<b>';
					echo '<a href="profile.php?user='.$view_topic_data['user_name'].'">' . $view_topic_data['user_name'] . '</a>';
					echo '</b>';
					echo '</small>';
					echo '</p>';
				?>
            </div>
        </div>
        <?php
			if (isset($_SESSION['learnOffice_uname']))
			{
				$q = "SELECT * FROM users WHERE user_name = '".$_SESSION['learnOffice_uname']."' AND user_role = 'Administrator' || 'Contributor'";
				$admin = mysql_query($q);
				$row = mysql_num_rows($admin);
				
				if ($row == 1) {
					echo '
					<div class="panel">
        				<div class="title3">Topic Actions</div>
            			<div class="panel-border3">
           					 - <img src="images/small_icons/icon_delete.gif" width="10px" /> <a href="remove_topic.php?topic_id='.$_GET['topic_id'].'">Remove Topic</a><br>
            				 - <img src="images/small_icons/icon_edit.gif" width="10px" /> <a href="update_topic.php?topic_id='.$_GET['topic_id'].'&topic='.$_GET['topic'].'">Update Topic Title & Content</a>
           				</div>
        			</div>
					';
				}
			}
		?>
        </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>