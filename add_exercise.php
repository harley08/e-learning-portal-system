<?php
	require('db/db.php');
	session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Add Exercise</title>
<link rel="stylesheet" href="css/styles.css">
</head>

<body>
	<?php
		include('header/header.php');
	?>
    <div class="body-width-80">
    	<div id="left">
        	<?php
				include('sidebars/left/left.php');
			?>
        </div>
        <div id="right">
        	<?php
				include('sidebars/right/right.php');
			?>
        </div>
        <div id="center">
        <div class="panel">
        	<div class="title3">Add New Exercise</div>
            <?php
				if (isset($_POST['exercise_title'])) {
					$exercise_title = $_POST['exercise_title'];
					$exercise_content = $_POST['exercise_content'];
					
					$exercise_title = stripslashes($exercise_title);
					$exercise_content = stripslashes($exercise_content);
					
					$exercise_title = mysql_real_escape_string($exercise_title);
					$exercise_content = mysql_real_escape_string($exercise_content);
					
					$lesson_id = $_GET['lesson_id'];
					$exercise_date_posted = date("Y-m-d H:i:s");
					$username = $_SESSION['learnOffice_uname'];
					$query_add_exercise = "INSERT INTO exercises (lesson_id, user_name, exercise_name, exercise_content, exercise_date_posted) VALUES ($lesson_id, '$username', '$exercise_title', '$exercise_content', '$exercise_date_posted')";
					$result_add_exercise = mysql_query($query_add_exercise);
					
					if ($result_add_exercise) {
						echo '
							<div class="alert-success">New Exercises Successfully Added!</div>
						';
					} else {
						echo '
							<div class="alert-warning">Cant Add New Exercise. Contact Administrator.</div>
						';
					}
				}
			?>
            <div class="panel-border3">
            	<form method="post" action="">
                	Title:<br>
                    <input type="text" name="exercise_title" required /><br><br>
                    Content:<br>
                    <textarea name="exercise_content" required ></textarea><br><br>
                    <input type="submit" value="Add Exercise" /> | <img src="images/small_icons/images/icon_topic_attach.gif" /> <a target="new" href="upload_screenshot.php">Attach Image/ScreenShot</a>
                </form>
            </div>
        </div>
    </div>
    </div>
    <?php
		include('footer/footer.php');
	?>
</body>
</html>